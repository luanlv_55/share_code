package controllers

import javax.inject.{Inject, Singleton}

import com.sksamuel.scrimage.{Format, Image}
import controllers.Auth._
import models.Models._
import models.Models.logger
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{JsValue, Json, _}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import reactivemongo.api.indexes.{IndexType, Index}
import reactivemongo.api.{Cursor, QueryOpts}
import reactivemongo.bson._
import reactivemongo.core.commands.{Match, Aggregate, Count}
import services.UUIDGenerator
import reactivemongo.api.gridfs.Implicits._


//----send email----------------------------------

//----send email----------------------------------


import models.Formats._

import scala.concurrent.Future

@Singleton
class Users @Inject()(uuidGenerator: UUIDGenerator)
    extends Controller
    with MongoController
    with Secured
{
  private final val logger = Logger

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //

  import models._
  import models.Models._


  def createUser = Action.async(parse.json) {
    request =>
      request.body.validate[UserValid] match {
        case JsSuccess(user, _) =>
          collectionUser
              .find(Json.obj("email" -> user.email))
              .cursor[JsObject].collect[List]().map(_.headOption) map {
            _ match {
              case None =>
                collectionUsers.insert(user).map {
                  lastError => logger.debug(s"Successfully inserted with LastError: $lastError")
                }
                Created
              case Some(userExist) =>
                BadRequest("Email exist")
            }
          }
        case err@JsError(_) =>
          logger.info("error")
          Future.successful(BadRequest("invalid json"))
      }
  }

  def updateUser = Action.async(parse.json) {
    request =>
      /*
       * request.body is a JsValue.
       * There is an implicit Writes that turns this JsValue as a JsObject,
       * so you can call insert() with this JsValue.
       * (insert() takes a JsObject as parameter, or anything that can be
       * turned into a JsObject using a Writes.)
       */

      request.body.validate[UserValid].map {
        user =>
          // `user` is an instance of the case class `models.User`
          collectionUsers.update(Json.obj("_id" -> user._id), user).map {
            lastError =>
              logger.debug(s"Successfully updated with LastError: $lastError")
              Created(s"User Updated")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }


  def list(page: Int, perPage: Int, searchText: String, searchTextMssv: String) = Action.async {
    // let's do our query
    val cursor: Cursor[UserValid] = collectionUsers.
        // find all
        find(Json.obj("name" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"), "mssv" -> Json.obj("$regex" ->  (".*" + searchTextMssv + ".*"), "$options" -> "-i"))).
        // pagination
        options(QueryOpts(page * perPage)).
        // sort them by creation date
        sort(Json.obj("mssv" -> 1)).
        // perform the query and get a cursor of JsObject
        cursor[UserValid]
    // gather all the JsObjects in a list
    val futureUsersList: Future[Seq[UserValid]] = cursor.collect[Seq](perPage)
    logger.info(futureUsersList.toString)
    // transform the list into a JsArray
    val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
      Json.arr(users)
    }
    // everything's ok! Let's reply with the array
    futurePersonsJsonArray.map {
      users =>
        Ok(users(0))
    }
  }

  /** The total number of messages */
  def countUsers(searchText: String, searchTextMssv: String) = Action.async {
    request => {
      val query = Json.obj("name" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"), "mssv" -> Json.obj("$regex" ->  (".*" + searchTextMssv + ".*"), "$options" -> "-i"))
      collectionUsers.find(query).cursor[JsObject].collect[List]() map {
        case objects => Ok(objects.length.toString)
      }
    }
  }

  def findUsers = Action.async {
    // let's do our query
    // find all
    val cursor: Cursor[UserValid] = collectionUsers.
        find(Json.obj()).
        // sort them by creation date
        sort(Json.obj("created" -> -1)).
        // perform the query and get a cursor of JsObject
        cursor[UserValid]

    // gather all the JsObjects in a list
    val futureUsersList: Future[List[UserValid]] = cursor.collect[List]()

    // transform the list into a JsArray
    val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
      Json.arr(users)
    }
    // everything's ok! Let's reply with the array
    futurePersonsJsonArray.map {
      users =>
        Ok(users(0))
    }
  }

  def findUser(id: String) = Action.async {
    val cursor: Cursor[UserValid] = collectionUsers.
        // find
        find(Json.obj("_id" -> BSONObjectID(id))).
        // perform the query and get a cursor of JsObject
        cursor[UserValid]

    // gather all the JsObjects q a listq
    val futureUsersList: Future[List[UserValid]] = cursor.collect[List]()

    // transform the list into a JsArray
    val futurePersonsJsonArray: Future[UserValid] = futureUsersList.map { users =>
      users.head
    }
    // everything's ok! Let's reply with the array
    futurePersonsJsonArray.map {
      user =>
        Ok(Json.format[UserValid].writes(user))
    }
  }

  def findUser2(email: String) = Action.async {
    val cursor: Cursor[UserValid] = collectionUsers.
        // find
        find(Json.obj("email" -> email)).
        // perform the query and get a cursor of JsObject
        cursor[UserValid]

    // gather all the JsObjects q a listq
    val futureUsersList: Future[List[UserValid]] = cursor.collect[List]()

    // transform the list into a JsArray
    val futurePersonsJsonArray: Future[UserValid] = futureUsersList.map { users =>
      users.head
    }
    // everything's ok! Let's reply with the array
    futurePersonsJsonArray.map {
      user =>
        Ok(Json.format[UserValid].writes(user))
    }
  }

  def deleteUser(email: String) = Action.async {
    logger.info("==>" + email)
    val futureRemove = collectionUsers.remove(Json.obj("email" -> email))
    futureRemove.map {
      result => Ok("Delete token Ok")
    }
  }

  def deleteToken(email: String) = Action.async {
    logger.info("==>" + email)
    val futureRemove = collectionToken.remove(Json.obj("email" -> email))
    futureRemove.map {
      result => Ok("Delete token Ok")
    }
  }

  def saveAttachment2(id: String) = Action.async(gridFSBodyParser(gridFS)) { request =>
    // here is the future file!
    val futureFile = request.body.files.head.ref

    val groupImg = java.util.UUID.randomUUID().toString()
    // when the upload is complete, we add the article id to the file entry (in order to find the attachments of the article)
      val futureUpdate = for {
        file <- futureFile
        // here, the file is completely uploaded, so it is time to update the user
        updateResult <- {
          gridFS.files.update(
            BSONDocument("_id" -> file.id),
            BSONDocument("$set" -> BSONDocument("metadata" ->
                BSONDocument("user" -> id,
                  "group" -> groupImg,
                  "size" -> "thumb"))))

        }
      } yield updateResult
      futureUpdate.map {
        case _ => Ok(groupImg)
      }.recover {
        case e => InternalServerError(e.getMessage())
      }
  }


  def saveAttachment(id: String) = Action.async(gridFSBodyParser(gridFS)) { request =>
    // here is the future file!
    val futureFile = request.body.files.head.ref

    val groupImg = java.util.UUID.randomUUID().toString()
    // when the upload is complete, we add the article id to the file entry (in order to find the attachments of the article)
    if (!request.body.files.head.contentType.toString.contains("image")) {
      Future.successful(BadRequest("Not is image"))
    } else {
      val futureUpdate = for {
        file <- futureFile
        // here, the file is completely uploaded, so it is time to update the user
        updateResult <- {
          val iterator = gridFS.enumerate(file).run(Iteratee.consume[Array[Byte]]())
          iterator.flatMap {
            bytes => {
              // Create resized image
              val enumerator: Enumerator[Array[Byte]] = Enumerator.outputStream(
                out => {
                  Image(bytes).bound(150, 185).writer(Format.JPEG).withCompression(90).write(out)
                }
              )
              val data = DefaultFileToSave(
                filename = file.filename,
                contentType = file.contentType,
                //uploadDate = Some(DateTime.now().getMillis),
                metadata = file.metadata ++ BSONDocument(
                  "user" -> id,
                  "group" -> groupImg,
                  "size" -> "thumb"
                )
              )

              gridFS.save(enumerator, data).map {
                image => Some(image)
              }
            }
          }

        }
      } yield updateResult
      futureUpdate.map {
        case _ => Ok(groupImg)
      }.recover {
        case e => InternalServerError(e.getMessage())
      }
    }

  }


  def listAttachment(id: String) = Action.async {
    val cursor: Cursor[FileInfo] = collectionFiles.
        find(Json.obj("metadata.group" -> id)).
        cursor[FileInfo]
    val futureFilesList: Future[List[FileInfo]] = cursor.collect[List]()

    val futureFilesJsonArray: Future[JsArray] = futureFilesList.map { files =>
      Json.arr(files)
    }
    futureFilesJsonArray.map {
      files =>
        Ok(files(0))
    }
  }



  def getAttachment(id: String, size: String) = Action.async { request =>
    // find the matching attachment, if any, and streams it to the client
    val file = gridFS.find(BSONDocument("metadata.group" -> id, "metadata.size" -> size))
    request.getQueryString("inline") match {
      case Some("true") => serve(gridFS, file, CONTENT_DISPOSITION_INLINE)
      case _ => serve(gridFS, file)
    }
  }


  def removeAttachment(uuid: String) = WithAuth {user =>
    _ =>
      val id = collectionFiles.find(Json.obj("metadata.group" -> uuid)).toString
      //gridFS.remove(BSONObjectID(uuid)).map(_ => Ok).recover { case _ => InternalServerError }
      val id2 = collectionFiles
          .find(Json.obj("metadata.group" ->  uuid))
          .cursor[JsObject].collect[List]().map(_.headOption).map {
        _ match {
          case None =>
            Future(BadRequest("Email not exist"))
          case Some(tokenx) => {
            val bid = ((tokenx \ "_id") \ "$oid").toString.slice(1,((tokenx \ "_id") \ "$oid").toString.length -1)
            gridFS.remove(BSONObjectID(bid)).map(_ => Ok).recover { case _ => InternalServerError }
          }
        }
      }
      Future.successful { Ok }
  }

  def mssvExists(mssv: String) = Action.async { implicit request =>
    findUserByMssv(Json.obj("mssv" -> mssv)) map {
      _ match {
        case None => Ok(Json.obj("isUnique" -> true))
        case Some(user) => Ok(Json.obj("isUnique" -> false))
      }
    }
  }

  def emailExists(email: String) = Action.async { implicit request =>
    findUserByEmail(Json.obj("email" -> email)) map {
      _ match {
        case None => Ok(Json.obj("isUnique" -> true))
        case Some(user) => Ok(Json.obj("isUnique" -> false))
      }
    }
  }

}
