package controllers

import javax.inject.{Inject, Singleton}

import com.sksamuel.scrimage.{Format, Image}
import controllers.Auth._
import models.Models._
import models.Models.logger
import models.{LuanVan, Teacher}
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{JsValue, Json, _}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import reactivemongo.api.{Cursor, QueryOpts}
import reactivemongo.bson._
import reactivemongo.core.commands.{Match, Aggregate, Count}
import services.UUIDGenerator
import reactivemongo.api.gridfs.Implicits._

//----send email----------------------------------

//----send email----------------------------------


import models.Formats._

import scala.concurrent.Future

@Singleton
class Luanvan @Inject()(uuidGenerator: UUIDGenerator)
    extends Controller
    with MongoController
    with Secured {
    private final val logger = Logger


    def countLuanvan(searchText: String, trangthai: String, malv: String, sinhvien: String, giaovien: String) = Action.async {
        request => {
            if (trangthai == ""){
                val query = Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                                    "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                                    "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                                    "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"))
                collectionLuanvan.find(query).cursor[JsObject].collect[List]() map {
                    case objects => Ok(objects.length.toString)
                }
            } else if (trangthai == "true"){
                val query = Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                                    "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                                    "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                                    "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                                    "chapnhan" -> Json.obj("$eq" ->  true))
                collectionLuanvan.find(query).cursor[JsObject].collect[List]() map {
                    case objects => Ok(objects.length.toString)
                }
            } else if (trangthai == "false"){
                val query = Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                                    "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                                    "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                                    "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                                    "chapnhan" -> Json.obj("$eq" ->  false))
                collectionLuanvan.find(query).cursor[JsObject].collect[List]() map {
                    case objects => Ok(objects.length.toString)
                }
            } else {
                val query = Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                                    "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                                    "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                                    "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                                    "chapnhan" -> Json.obj("$eq" ->  JsNull))
                collectionLuanvan.find(query).cursor[JsObject].collect[List]() map {
                    case objects => Ok(objects.length.toString)
                }
            }
        }
    }
    
    def listLuanvan(page: Int, perPage: Int, searchText: String, trangthai: String, malv: String, sinhvien: String, giaovien: String) = Action.async {
        // let's do our query
        if( trangthai == "") {
            val cursor: Cursor[LuanVan] = collectionLuanvan.
                // find all
                find(Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                            "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                            "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                            "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"))).
                // pagination
                options(QueryOpts(page * perPage)).
                // sort them by creation date
                sort(Json.obj("ten" -> 1)).
                // perform the query and get a cursor of JsObject
                cursor[LuanVan]
            // gather all the JsObjects in a list
            val futureUsersList: Future[Seq[LuanVan]] = cursor.collect[Seq](perPage)
            logger.info(futureUsersList.toString)
            // transform the list into a JsArray
            val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
                Json.arr(users)
            }
            // everything's ok! Let's reply with the array
            futurePersonsJsonArray.map {
                users =>
                    Ok(users(0))
            }
        } else if( trangthai == "true") {
            val cursor: Cursor[LuanVan] = collectionLuanvan.
                // find all
                find(Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                            "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                            "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                            "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                            "chapnhan" -> Json.obj("$eq" ->  true))).
                // pagination
                options(QueryOpts(page * perPage)).
                // sort them by creation date
                sort(Json.obj("ten" -> 1)).
                // perform the query and get a cursor of JsObject
                cursor[LuanVan]
            // gather all the JsObjects in a list
            val futureUsersList: Future[Seq[LuanVan]] = cursor.collect[Seq](perPage)
            logger.info(futureUsersList.toString)
            // transform the list into a JsArray
            val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
                Json.arr(users)
            }
            // everything's ok! Let's reply with the array
            futurePersonsJsonArray.map {
                users =>
                    Ok(users(0))
            }
        } else if( trangthai == "false") {
            val cursor: Cursor[LuanVan] = collectionLuanvan.
                // find all
                find(Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                            "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                            "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                            "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                            "chapnhan" -> Json.obj("$eq" ->  false))).
                // pagination
                options(QueryOpts(page * perPage)).
                // sort them by creation date
                sort(Json.obj("ten" -> 1)).
                // perform the query and get a cursor of JsObject
                cursor[LuanVan]
            // gather all the JsObjects in a list
            val futureUsersList: Future[Seq[LuanVan]] = cursor.collect[Seq](perPage)
            logger.info(futureUsersList.toString)
            // transform the list into a JsArray
            val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
                Json.arr(users)
            }
            // everything's ok! Let's reply with the array
            futurePersonsJsonArray.map {
                users =>
                    Ok(users(0))
            }
        } else {
            val cursor: Cursor[LuanVan] = collectionLuanvan.
                // find all
                find(Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"),
                            "ma" -> Json.obj("$regex" ->  (".*" + malv + ".*"), "$options" -> "-i"),
                            "sinhvien" -> Json.obj("$regex" ->  (".*" + sinhvien + ".*"), "$options" -> "-i"),
                            "giaovien" -> Json.obj("$regex" ->  (".*" + giaovien + ".*"), "$options" -> "-i"),
                            "chapnhan" -> Json.obj("$eq" ->  JsNull))).
                // pagination
                options(QueryOpts(page * perPage)).
                // sort them by creation date
                sort(Json.obj("ten" -> 1)).
                // perform the query and get a cursor of JsObject
                cursor[LuanVan]
            // gather all the JsObjects in a list
            val futureUsersList: Future[Seq[LuanVan]] = cursor.collect[Seq](perPage)
            logger.info(futureUsersList.toString)
            // transform the list into a JsArray
            val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
                Json.arr(users)
            }
            // everything's ok! Let's reply with the array
            futurePersonsJsonArray.map {
                users =>
                    Ok(users(0))
            }
        }
    }

    def createLV = Action.async(parse.json) {
        request =>
            request.body.validate[LuanVan] match {
                case JsSuccess(luanvan, _) =>
                    collectionLuanvan.insert(luanvan).map {
                        lastError =>
                            logger.debug(s"Successfully inserted with LastError: $lastError")
                            Created
                    }
                    collectionUsers.update(Json.obj("_id" -> BSONObjectID(luanvan.sinhvienID)), Json.obj("$set" -> Json.obj("luanvan" -> true)))
                    Future.successful(Ok)
                case err@JsError(_) =>
                    logger.info("error")
                    Future.successful(BadRequest("invalid json"))
            }
    }

    def getLV(uuid: String) = Action.async {
        logger.info(uuid)
        val cursor: Cursor[LuanVan] = collectionLuanvan.
            // find
            find(Json.obj("sinhvienID" -> uuid)).
            // perform the query and get a cursor of JsObject
            cursor[LuanVan]

        // gather all the JsObjects q a listq
        val futureUsersList: Future[List[LuanVan]] = cursor.collect[List]()

        // transform the list into a JsArray
        val futurePersonsJsonArray: Future[LuanVan] = futureUsersList.map { luanvan =>
            luanvan.head
        }
        // everything's ok! Let's reply with the array
        futurePersonsJsonArray.map {
            user =>
                Ok(Json.format[LuanVan].writes(user))
        }
    }

    def luanvanok = WithAuth(parse.json) { _ =>
        implicit request =>
            collectionLuanvan.update(Json.obj("ma" -> request.body \ "ma"),
                Json.obj("$set" -> Json.obj("chapnhan" -> true)))
            Future.successful(Ok)
    }

    def luanvanhuy = WithAuth(parse.json) { _ =>
        implicit request =>
            collectionLuanvan.update(Json.obj("ma" -> request.body \ "ma"),
                Json.obj("$set" -> Json.obj("chapnhan" -> false)))
            Future.successful(Ok)
    }

    def deleteLV(id: String, sid: String) = WithAuth { user =>
        request =>
            collectionUsers.update(Json.obj("_id" -> BSONObjectID(sid)),
                Json.obj("$set" -> Json.obj("luanvan" -> false)))
            collectionLuanvan.remove(Json.obj("_id" -> BSONObjectID(id))) flatMap { lastError =>
                if (lastError.ok)
                    Future.successful(Ok)
                else
                    Future.successful { BadRequest(lastError.stringify) }
            }
    }
}
