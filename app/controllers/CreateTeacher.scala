package controllers

import javax.inject.{Inject, Singleton}

import com.sksamuel.scrimage.{Format, Image}
import models.Models._
import models.Teacher
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{JsValue, Json, _}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import reactivemongo.api.{Cursor, QueryOpts}
import reactivemongo.bson._
import reactivemongo.core.commands.Count
import services.UUIDGenerator
import reactivemongo.api.gridfs.Implicits._

//----send email----------------------------------

//----send email----------------------------------


import models.Formats._

import scala.concurrent.Future

@Singleton
class CreateTeacher @Inject()(uuidGenerator: UUIDGenerator)
    extends Controller
    with MongoController
    with Secured {
    private final val logger = Logger

    def createTeacher = Action.async(parse.json) {
        request =>
            request.body.validate[Teacher] match {
                case JsSuccess(user, _) =>
                    collectionTeachers
                        .find(Json.obj("email" -> user.email))
                        .cursor[JsObject].collect[List]().map(_.headOption) map {
                        _ match {
                            case None =>
                                collectionTeachers.insert(user).map {
                                    lastError => logger.debug(s"Successfully inserted with LastError: $lastError")
                                }
                                Created
                            case Some(userExist) =>
                                BadRequest("Email exist")
                        }
                    }
                case err@JsError(_) =>
                    logger.info("error")
                    Future.successful(BadRequest("invalid json"))
            }
    }

    /** The total number of messages */
    def countTeachers(searchText: String) = Action.async {
        request => {
            val query = Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"))
            collectionTeachers.find(query).cursor[JsObject].collect[List]() map {
                case objects => Ok(objects.length.toString)
            }
        }
    }


    def listTeacher(page: Int, perPage: Int, searchText: String) = Action.async {
        // let's do our query
        val cursor: Cursor[Teacher] = collectionTeachers.
            // find all
            find(Json.obj("ten" -> Json.obj("$regex" ->  (".*" + searchText + ".*"), "$options" -> "-i"))).
            // pagination
            options(QueryOpts(page * perPage)).
            // sort them by creation date
            sort(Json.obj("ten" -> 1)).
            // perform the query and get a cursor of JsObject
            cursor[Teacher]
        // gather all the JsObjects in a list
        val futureUsersList: Future[Seq[Teacher]] = cursor.collect[Seq](perPage)
        logger.info(futureUsersList.toString)
        // transform the list into a JsArray
        val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
            Json.arr(users)
        }
        // everything's ok! Let's reply with the array
        futurePersonsJsonArray.map {
            users =>
                Ok(users(0))
        }
    }

    def updateUser = Action.async(parse.json) {
        request =>
            /*
             * request.body is a JsValue.
             * There is an implicit Writes that turns this JsValue as a JsObject,
             * so you can call insert() with this JsValue.
             * (insert() takes a JsObject as parameter, or anything that can be
             * turned into a JsObject using a Writes.)
             */

            request.body.validate[Teacher].map {
                user =>
                    // `user` is an instance of the case class `models.User`
                    collectionTeachers.update(Json.obj("_id" -> user._id), user).map {
                        lastError =>
                            logger.debug(s"Successfully updated with LastError: $lastError")
                            Created(s"User Updated")
                    }
            }.getOrElse(Future.successful(BadRequest("invalid json")))
    }

    def findTeacher(id: String) = Action.async {
        val cursor: Cursor[Teacher] = collectionTeachers.
            // find
            find(Json.obj("_id" -> BSONObjectID(id))).
            // perform the query and get a cursor of JsObject
            cursor[Teacher]

        // gather all the JsObjects q a listq
        val futureUsersList: Future[List[Teacher]] = cursor.collect[List]()

        // transform the list into a JsArray
        val futurePersonsJsonArray: Future[Teacher] = futureUsersList.map { users =>
            users.head
        }
        // everything's ok! Let's reply with the array
        futurePersonsJsonArray.map {
            user =>
                Ok(Json.format[Teacher].writes(user))
        }
    }

    def findTeacher2(email: String) = Action.async {
        val cursor: Cursor[Teacher] = collectionTeachers.
            // find
            find(Json.obj("email" -> email)).
            // perform the query and get a cursor of JsObject
            cursor[Teacher]

        // gather all the JsObjects q a listq
        val futureUsersList: Future[List[Teacher]] = cursor.collect[List]()

        // transform the list into a JsArray
        val futurePersonsJsonArray: Future[Teacher] = futureUsersList.map { users =>
            users.head
        }
        // everything's ok! Let's reply with the array
        futurePersonsJsonArray.map {
            user =>
                Ok(Json.format[Teacher].writes(user))
        }
    }

    def deleteTeacher(email: String) = Action.async {
        val futureRemove = collectionTeachers.remove(Json.obj("email" -> email))
        futureRemove.map {
            result => Ok("delete Ok")
        }
    }

}
