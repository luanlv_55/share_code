package services

import

com.typesafe.plugin.{MailerPlugin, MailerAPI}
import play.api.libs.json.{Json, JsValue}
import play.api.mvc.Controller
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.Logger

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{ JsValue, Json }
import play.api.mvc._
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import scala.concurrent.{ExecutionContext, Future}

import org.apache.commons.mail.EmailAttachment
//----send email----------------------------------


object EmailService extends Controller with MongoController{
    final val logger = Logger
    val host = "http://localhost:9000"
//    val host = "http://quanly2.herokuapp.com"
    import models.Models._
    def sendEmail(toAddress: String, subject: String, body: String) {
        //    val mail =  use[MailerPlugin].email
        //    mail.addFrom(Play.current.configuration.getString("smtp.user").getOrElse(""))
        //    mail.addRecipient(toAddress)
        //    mail.setSubject(subject)
        //    mail.send(body)
        val mail: MailerAPI = play.Play.application.plugin(classOf[MailerPlugin]).email
        mail.setSubject(subject)
        mail.setRecipient(toAddress)
        mail.setFrom("somefromadd@email.com")

        val newBody = "Vào link sau để đặt mật khẩu:" + body
        mail.send(newBody)
    }



    def sendEmailWithToken(toAddress: String, action: String, subject: String) = {
        val token = java.util.UUID.randomUUID().toString()
        val jsonString = "{\"email\" : \""+toAddress+"\",\"action\":\"" + action +"\",\"token\":\""+token+"\"}"
        val jsonObject: JsValue = Json.parse(jsonString)

        collectionToken.insert(jsonObject).map {
            lastError =>
                logger.debug(s"Successfully create token with LastError: $lastError")
        }

        sendEmail(toAddress, subject, host + "/setpassword/" + token)
    }

    def sendEmailWithTokenReset(toAddress: String, subject: String, newToken: String) = {
        collectionToken.update(Json.obj("email" -> toAddress), Json.obj("$set" -> Json.obj("action" -> "reset")))
        collectionToken.update(Json.obj("email" -> toAddress), Json.obj("$set" -> Json.obj("token" -> newToken)))
        sendEmail(toAddress, subject, host + "/setnewpassword/" + newToken)
    }
}
