package models

import com.sun.corba.se.spi.ior.ObjectId
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._

import play.api.libs.functional.syntax._
import play.api.libs.json.{Json, Writes, JsPath, Reads}
import reactivemongo.bson.{BSONDateTime, BSONDocument, BSONDocumentReader, BSONObjectID}
import play.modules.reactivemongo.json.BSONFormats._

case class User(email: String, password: String, role: String)

object JsonFormats {
    import play.api.libs.json.Json
    implicit val userFormat = Json.format[User]
}

case class UserValid(_id: Option[BSONObjectID], avatarID: Option[String], mssv: String, name: String, email: String, nganh: String, khoa: String, dienthoai: Option[String], diachi: Option[String], active: Boolean, luanvan: Boolean)
object UserValid{
    implicit val ackReads: Reads[UserValid] = (
        (JsPath \ "_id").readNullable[BSONObjectID] and
            (JsPath \ "avatarID").readNullable[String] and
            (JsPath \ "mssv").read[String] (Reads.verifying[String]("""^[0-9]{8}$""".r.pattern.matcher(_).matches)) and
            (JsPath \ "name").read[String] and
            (JsPath \ "email").read[String] (Reads.verifying[String]("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r.pattern.matcher(_).matches)) and
            (JsPath \ "nganh").read[String] and
            (JsPath \ "khoa").read[String] and
            (JsPath \ "dienthoai").readNullable[String] and
            (JsPath \ "diachi").readNullable[String] and
            (JsPath \ "active").read[Boolean] and
            (JsPath \ "luanvan").read[Boolean]
        )(UserValid.apply _)
    //(Reads.verifying[String]("""^[0-9]{1}$""".r.pattern.matcher(_).matches)) and
    implicit val ackWrites: Writes[UserValid] = (
        (JsPath \ "_id").writeNullable[BSONObjectID] and
            (JsPath \ "avatarID").writeNullable[String] and
            (JsPath \ "mssv").write[String] and
            (JsPath \ "name").write[String] and
            (JsPath \ "email").write[String] and
            (JsPath \ "nganh").write[String] and
            (JsPath \ "khoa").write[String] and
            (JsPath \ "dienthoai").writeNullable[String] and
            (JsPath \ "diachi").writeNullable[String] and
            (JsPath \ "active").write[Boolean] and
            (JsPath \ "luanvan").write[Boolean]
        )(unlift(UserValid.unapply))
}

case class Metadata(user: String, group: String, size: String)
case class FileInfo(_id: Option[BSONObjectID], filename: String, contentType: String, metadata: Metadata)
object FileInfo{
    implicit val metadataReads: Reads[Metadata] = (
        (JsPath \ "user").read[String] and
            (JsPath \ "group").read[String] and
            (JsPath \ "size").read[String]
        )(Metadata.apply _)
    implicit val metadataWrites: Writes [Metadata] = (
        (JsPath \ "user").write[String] and
            (JsPath \ "group").write[String] and
            (JsPath \ "size").write[String]
        )(unlift(Metadata.unapply))

    implicit val fileIntoReads: Reads[FileInfo] = (
        (JsPath \ "_id").read[Option[BSONObjectID]] and
            (JsPath \ "filename").read[String] and
            (JsPath \ "contentType").read[String] and
            (JsPath \ "metadata").read[Metadata]
        )(FileInfo.apply _)
    //(Reads.verifying[String]("""^[0-9]{1}$""".r.pattern.matcher(_).matches)) and
    implicit val fileInfoWrites: Writes[FileInfo] = (
        (JsPath \ "_id").write[Option[BSONObjectID]] and
            (JsPath \ "filename").write[String] and
            (JsPath \ "contentType").write[String] and
            (JsPath \ "metadata").write[Metadata]
        )(unlift(FileInfo.unapply))
}


case class Teacher(_id: Option[BSONObjectID], avatarID: Option[String], ten: String, chucdanh: String, noicongtac: String, dienthoai: Option[String], email: String, diachi: Option[String], active: Boolean)
object Teacher{
    implicit val teacherReads: Reads[Teacher] = (
        (JsPath \ "_id").readNullable[BSONObjectID] and
            (JsPath \ "avatarID").readNullable[String] and
            (JsPath \ "ten").read[String] and
            (JsPath \ "chucdanh").read[String] and
            (JsPath \ "noicongtac").read[String] and
            (JsPath \ "dienthoai").readNullable[String] and
            (JsPath \ "email").read[String] (Reads.verifying[String]("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r.pattern.matcher(_).matches)) and
            (JsPath \ "diachi").readNullable[String] and
            (JsPath \ "active").read[Boolean]
        )(Teacher.apply _)
    implicit val teacherWrites: Writes[Teacher] = (
        (JsPath \ "_id").writeNullable[BSONObjectID] and
            (JsPath \ "avatarID").writeNullable[String] and
            (JsPath \ "ten").write[String] and
            (JsPath \ "chucdanh").write[String] and
            (JsPath \ "noicongtac").write[String] and
            (JsPath \ "dienthoai").writeNullable[String] and
            (JsPath \ "email").write[String] and
            (JsPath \ "diachi").writeNullable[String] and
            (JsPath \ "active").write[Boolean]
        )(unlift(Teacher.unapply))
}

case class Tokens(email: String, action: String, token: Option[String])
object Tokens{
    implicit val tokenReads: Reads[Tokens] = (
        (JsPath \ "email").read[String] and
            (JsPath \ "action").read[String] and
            (JsPath \ "token").readNullable[String]
        )(Tokens.apply _)
    implicit val tokenWrites: Writes[Tokens] = (
        (JsPath \ "email").write[String] and
            (JsPath \ "action").write[String] and
            (JsPath \ "token").writeNullable[String]
        )(unlift(Tokens.unapply))
}

case class LuanVan(_id: Option[BSONObjectID], giaovienID: String, sinhvienID: String, giaovien: String, sinhvien: String, ma: String, ten: String, gioithieu: String, fileID: String, chapnhan: Option[Boolean])
object LuanVan{
    implicit val luanvanReads: Reads[LuanVan] = (
        (JsPath \ "_id").readNullable[BSONObjectID] and
            (JsPath \ "giaovienID").read[String] and
            (JsPath \ "sinhvienID").read[String] and
            (JsPath \ "giaovien").read[String] and
            (JsPath \ "sinhvien").read[String] and
            (JsPath \ "ma").read[String] and
            (JsPath \ "ten").read[String] and
            (JsPath \ "gioithieu").read[String] and
            (JsPath \ "fileID").read[String] and
            (JsPath \ "chapnhan").readNullable[Boolean]
        )(LuanVan.apply _)
    implicit val luanvanWrites: Writes[LuanVan] = (
        (JsPath \ "_id").writeNullable[BSONObjectID] and
            (JsPath \ "giaovienID").write[String] and
            (JsPath \ "sinhvienID").write[String] and
            (JsPath \ "giaovien").write[String] and
            (JsPath \ "sinhvien").write[String] and
            (JsPath \ "ma").write[String] and
            (JsPath \ "ten").write[String] and
            (JsPath \ "gioithieu").write[String] and
            (JsPath \ "fileID").write[String] and
            (JsPath \ "chapnhan").writeNullable[Boolean]
        )(unlift(LuanVan.unapply))
}