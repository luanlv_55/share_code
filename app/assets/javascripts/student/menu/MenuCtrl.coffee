class MenuCtrl
    constructor: (@$log, @$location, @$rootScope) ->
        @$log.debug "constructing MenuController"

        @home =
            title: 'Home page'
            links:
                content: 'home page'
                url: '/student#/'
        @giaovien =
            title: 'quan ly giao vien'
            links:
                content: 'qan ly giao vien'
                url: '/student#/teachers'
        @luanvan =
            title: 'quan ly luan van'
            links:
                content: 'quan ly luan van'
                url: '/student#/luanvan'
        @detail =
            title: 'Thong tin sinh vien'
            links:
                content: 'Thong tin sinh vien'
                url: '/student#/detail'
        @resetpassword =
            title: 'Dat lai mat khau'
            links:
                content: 'Dat lai mat khau'
                url: '/student#/resetpassword'
controllersModule.controller('MenuCtrl', MenuCtrl)