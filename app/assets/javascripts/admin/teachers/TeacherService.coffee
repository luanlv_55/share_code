
class TeacherService
  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing TeacherService"

  listAllTeachers: () ->
    @$log.debug "listTeachers()"
    deferred = @$q.defer()

    @$http.get("/teachers")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Teachers - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Teachers - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  listTeachers: (page, perPage, searchText) ->
    @$log.debug "listTeachers()"
    deferred = @$q.defer()

    @$http.get("/teachers/#{page}/#{perPage}?searchText=#{searchText}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Teachers - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Teachers - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


  countTeachers: (searchText) ->
    @$log.debug "countTeachers()"
    deferred = @$q.defer()
    @$http.get("/teachers/count?searchText=#{searchText}")
    .success((data, status, headers) =>
      @$log.info("Successfully counted Teachers - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to count Teachers - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getTeacher: (uuid) ->
    @$log.debug "getTeacher()"
    deferred = @$q.defer()

    @$http.get("/teacher/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve Teacher - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve Teacher - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteTeacher: (uuid) ->
    @$log.debug "deleteTeacher()"
    deferred = @$q.defer()

    @$http.delete("/teacher/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove Teacher - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove Teacher - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteToken: (emailAccount) ->
    @$log.debug "deleteUser()"
    deferred = @$q.defer()
    @$http.delete("/api/auth/deleteTokenWithEmail/#{emailAccount}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createTeacher: (teacher) ->
    @$log.debug "createTeacher #{angular.toJson(teacher, true)}"
    deferred = @$q.defer()

    @$http.post('/teacher', teacher)
    .success((data, status, headers) =>
      @$log.info("Successfully created Teacher - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create teacher - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createAccount: (account) ->
    deferred = @$q.defer()
    @$http.post('/api/auth/user', account)
    .success((data, status, headers) =>
      @$log.info("Successfully created Account - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create Account - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  updateTeacher: (teacher) ->
    @$log.debug "updateTeacher #{angular.toJson(teacher, true)}"
    deferred = @$q.defer()

    @$http.post('/teacher/update', teacher)
    .success((data, status, headers) =>
      @$log.info("Successfully update Teacher - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to update teacher - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteAvatar: (uuid) ->
    @$log.debug "deleteAvatar()"
    deferred = @$q.defer()
    @$http.delete("/api/fileupload/delete/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully delete avatar - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to delete avatar - status #{status}")
      deferred.reject(data)
    )
    deferred.promise
  updateEmail: (userLogin) ->
    deferred = @$q.defer()
    @$http.post('/api/auth/userEmail', userLogin)
    .success((data, status, headers) =>
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      deferred.reject(data)
    )
    deferred.promise

  deleteTeacherAccount: (emailAccount) ->
    @$log.debug "deleteTeacher()"
    deferred = @$q.defer()
    @$http.delete("/api/auth/deleteUserWithEmail/#{emailAccount}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  generateUUID: () ->
    @$log.debug "generate UUID"
    deferred = @$q.defer()

    @$http.get("/randomUUID")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve UUID - status #{status}")
      deferred.resolve(data)
      @$log.debug "UUID #{data}"
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve UUID - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getLuanVan: (uuid) ->
    deferred = @$q.defer()

    @$http.get("/luanvan/teacher/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed luanvan - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list luanvan - status #{status}")
      deferred.reject(data)
    )
    deferred.promise
servicesModule.service('TeacherService', TeacherService)