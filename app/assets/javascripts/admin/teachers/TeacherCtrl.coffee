
class TeacherCtrl

    constructor: (@$log, @$location, @TeacherService) ->
        @$log.debug "constructing TeacherController"
        @loading = true
        @currentPage = 1
        @numPerPage = 10
        @maxSize = 5
        @bigTotalItems = 0
        @teachers = []
        @preSearchText = ""
        @searchText = ""
        @getAllTeachers()


    clear: () ->
        @searchText = ""
        @preSearchText = ""
        @loading = true
        @getAllTeachers()
    search: () ->
        @searchText = @preSearchText
        @loading = true
        @getAllTeachers()
        
    getAllTeachers: () ->
        @$log.debug "getAllTeachers()"
        @TeacherService.countTeachers(@searchText)
        .then(
            (data) =>
                @$log.debug "Promise returned Teachers count " + data
                @bigTotalItems = Math.ceil(data / @numPerPage)
        ,
            (error) =>
                @$log.error "Unable to get Teachers count: #{error}"
                @bigTotalItems = 0
        )

        @TeacherService.listTeachers(@currentPage - 1, @numPerPage, @searchText)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Teachers"
                @teachers = data
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to get Teachers: #{error}"
                @loading = false
        )


    pageNext: () ->
        @currentPage = @currentPage + 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllTeachers()

    pagePrev: () ->
        @currentPage = @currentPage - 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllTeachers()

    deleteTeacher: (email) ->
        @$log.debug "deleteTeacher(#{email})"
        @loading = true
        @TeacherService.deleteTeacher(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                @getAllTeachers()
                @TeacherService.deleteTeacherAccount(email)
                .then(
                    (data) =>
                        @$log.debug "delete teacher account ok!"
                ,
                    (error) =>
                        @$log.debug "delete teacher account error"
                )
                @TeacherService.deleteToken(email)
                .then(
                    (data) =>
                        @$log.debug "delete Token ok!"
                ,
                    (error) =>
                        @$log.debug "delete Token error"
                )
        ,
            (error) =>
                @$log.error "Unable to delete Teacher: #{error}"
        )

controllersModule.controller('TeacherCtrl', TeacherCtrl)