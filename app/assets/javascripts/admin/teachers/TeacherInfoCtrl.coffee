
class TeacherInfoCtrl

    constructor: (@$log, @$location, @$routeParams, @TeacherService, @UserService, @security, @$rootScope, @fileUpload ) ->
        @teacher = {}
        @user = {}
        @luanvan = []
        @getTeacher(@$routeParams.uuid)
        @file = {}
        @myFile = {}



    getTeacher: (uuid) ->
        @TeacherService.getTeacher(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                @teacher = data
                @TeacherService.getLuanVan(@teacher._id.$oid)
                .then(
                    (data) =>
                        console.log "get luanvan OK"
                        @luanvan = data
                ,
                    (error) =>
                        console.log "get luanvan Error"
                )
        ,
            (error) =>
                @$log.error "Unable to get Teacher: #{error}"
        )

    getLuanVan: (uuid) ->
        @TeacherService.getLuanVan(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} luan van"
                @luanvan = data
        ,
            (error) =>
                @$log.error "Unable to get luan van #{error}"
        )


controllersModule.controller('TeacherInfoCtrl', TeacherInfoCtrl)