
class ListLuanvanCtrl

    constructor: (@$log, @$location, @LuanvanService) ->
        @$log.debug "constructing LuanvanController"
        @loading = true
        @currentPage = 1
        @numPerPage = 10
        @maxSize = 5
        @bigTotalItems = 0
        @luanvan = []

        @preSearchText = ""
        @searchText = ""

        @preTrangthai = ""
        @trangthai = ""

        @preMalv = ""
        @malv = ""

        @preSinhvien = ""
        @sinhvien = ""

        @preGiaovien = ""
        @giaovien = ""
        @getAllLuanvan()


    clear: () ->
        @searchText = ""
        @preSearchText = ""
        @trangthai = ""
        @preTrangthai = ""
        @preMalv = ""
        @malv = ""
        @preSinhvien = ""
        @sinhvien = ""
        @preGiaovien = ""
        @giaovien = ""
        @loading = true
        @getAllLuanvan()
    search: () ->
        @searchText = @preSearchText
        @trangthai = @preTrangthai
        @sinhvien = @preSinhvien
        @giaovien = @preGiaovien
        @malv = @preMalv
        @loading = true
        @getAllLuanvan()
        
    getAllLuanvan: () ->
        @$log.debug "getAllLuanvan()"
        @LuanvanService.countLuanvan(@searchText, @trangthai, @malv, @sinhvien, @giaovien)
        .then(
            (data) =>
                @$log.debug "Promise returned Luanvan count " + data
                @bigTotalItems = Math.ceil(data / @numPerPage)
        ,
            (error) =>
                @$log.error "Unable to get Luanvan count: #{error}"
                @bigTotalItems = 0
        )

        @LuanvanService.listLuanvan(@currentPage - 1, @numPerPage, @searchText, @trangthai,  @malv, @sinhvien, @giaovien)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Luanvan"
                @luanvan = data
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to get Luanvan: #{error}"
                @loading = false
        )


    pageNext: () ->
        @currentPage = @currentPage + 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllLuanvan()

    pagePrev: () ->
        @currentPage = @currentPage - 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllLuanvan()

    deleteLuanvan: (email) ->
        @$log.debug "deleteLuanvan(#{email})"
        @loading = true
        @LuanvanService.deleteLuanvan(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Luanvan"
                @getAllLuanvan()
                @LuanvanService.deleteLuanvanAccount(email)
                .then(
                    (data) =>
                        @$log.debug "delete teacher account ok!"
                ,
                    (error) =>
                        @$log.debug "delete teacher account error"
                )
                @LuanvanService.deleteToken(email)
                .then(
                    (data) =>
                        @$log.debug "delete Token ok!"
                ,
                    (error) =>
                        @$log.debug "delete Token error"
                )
        ,
            (error) =>
                @$log.error "Unable to delete Luanvan: #{error}"
        )
    chapnhan: (malv) ->
        @tmp = {"ma": malv}
        @LuanvanService.chapnhan(@tmp)
        .then(
            (data) =>
                console.log "da chap nhan"
                @getAllLuanvan()
        ,
            (error) =>
                console.log "co loi"
        )
    huybo: (malv) ->
        @tmp = {"ma": malv}
        @LuanvanService.huybo(@tmp)
        .then(
            (data) =>
                console.log "da huy bo"
                @getAllLuanvan()
        ,
            (error) =>
                console.log "co loi"
        )
controllersModule.controller('ListLuanvanCtrl', ListLuanvanCtrl)