
dependencies = [
    'ngRoute',
    'ngCookies',
    'myApp.filters',
    'myApp.services',
    'myApp.controllers',
    'myApp.directives',
    'myApp.common',
    'myApp.routeConfig',
]

app = angular.module('myApp', dependencies)

angular.module('myApp.routeConfig', ['ngRoute', 'ngCookies'])
.config ($routeProvider) ->
    $routeProvider
    .when('/', {
                templateUrl: '/assets/teacher/home.html'
            })
    .when('/teachers', {
                templateUrl: '/assets/teacher/list2.html'
            })
    .when('/luanvan', {
                templateUrl: '/assets/teacher/list2.html'
            })
    .when('/detail', {
                templateUrl: '/assets/teacher/detail2.html'
            })
    .when('/teachers/detail/:uuid', {
                templateUrl: '/assets/teacher/tdetail.html'
            })
    .when('/resetpassword', {
                templateUrl: '/assets/teacher/resetpassword.html'
            })
    .otherwise({redirectTo: '/'})

.run (@$rootScope, @$location, security, @$cookies) ->
    @$rootScope.$on('$routeChangeStart',
        (event, next) ->
            changeMenu = () ->
                console.log "change Menu"
                @$rootScope.menu1 = @$rootScope.menu2 = @$rootScope.menu3 = @$rootScope.menu4 =@$rootScope.menu5 = @$rootScope.menu6 = ''
                if @$rootScope.active = @$location.$$path is '/'
                    @$rootScope.menu1 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,9) is '/teachers'
                    @$rootScope.menu2 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,9) is '/luanvan'
                    @$rootScope.menu3 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,9) is '/detail'
                    @$rootScope.menu4 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,16) is '/resetpassword'
                    @$rootScope.menu5 = 'submenu-active'
            security.isAuthorized()
            .success(
                () =>
                    changeMenu()
                    @$rootScope.security = {
                        'isAuthorized': true,
                        'email': security.email
                    })
            .error(
                () =>
                    changeMenu()
                    @$rootScope.security = {
                        'isAuthorized': false,
                        'email': ''
                    }
            )

            @role = ''
            if(@$cookies.role isnt undefined)
                @role = @$cookies.role
            if (next.requireAuthorization isnt undefined)
                if (@role not in next.requireAuthorization)
                    if @role is ''
                        $location.path('/signin')
                    else
                        $location.path('/')
    )

@commonModule = angular.module('myApp.common', [])
@controllersModule = angular.module('myApp.controllers', [])
@servicesModule = angular.module('myApp.services', [])
@modelsModule = angular.module('myApp.models', [])
@directivesModule = angular.module('myApp.directives', [])
@filtersModule = angular.module('myApp.filters', [])
