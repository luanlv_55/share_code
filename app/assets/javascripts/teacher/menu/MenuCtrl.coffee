class MenuCtrl
    constructor: (@$log, @$location, @$rootScope) ->
        @$log.debug "constructing MenuController"

        @home =
            title: 'Home page'
            links:
                content: 'home page'
                url: '/teacher#/'
        @giaovien =
            title: 'quan ly giao vien'
            links:
                content: 'qan ly giao vien'
                url: '/teacher#/teachers'
        @luanvan =
            title: 'quan ly luan van'
            links:
                content: 'qqan ly luan van'
                url: '/teacher#/luanvan'
        @detail =
            title: 'Thong tin ca nhan'
            links:
                content: 'Thong tin ca nhan'
                url: '/teacher#/detail'
        @resetpassword =
            title: 'Dat lai mat khau'
            links:
                content: 'Dat lai mat khau'
                url: '/teacher#/resetpassword'
controllersModule.controller('MenuCtrl', MenuCtrl)