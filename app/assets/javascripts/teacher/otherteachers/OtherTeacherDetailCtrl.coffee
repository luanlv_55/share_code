
class OtherTeacherDetailCtrl

    constructor: (@$log, @$location, @$routeParams, @OtherTeacherService, @security) ->
        @teacher = {}
        @luanvan = []
        @getTeacher(@$routeParams.uuid)

    getTeacher: (uuid) ->
        @OtherTeacherService.getTeacher(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                @teacher = data
        ,
            (error) =>
                @$log.error "Unable to get Teacher: #{error}"
        )

    getLuanVan: (uuid) ->
        @OtherTeacherService.getLuanVan(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} luan van"
                @luanvan = data
        ,
            (error) =>
                @$log.error "Unable to get luan van #{error}"
        )


controllersModule.controller('OtherTeacherDetailCtrl', OtherTeacherDetailCtrl)